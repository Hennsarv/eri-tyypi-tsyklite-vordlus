﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 11, 23, 217, 6, 4, 20, 5 };
            // kaks samaväärset tsüklit - for ja foreach
            // tegelik foreach kasutab enamvähem TÄPSELT sellist FOR tsüklit
            // FOR
            IEnumerator<int> x = ((IEnumerable<int>)arvud).GetEnumerator();
            for (x.Reset(); x.MoveNext(); )
            {
                int i = x.Current;
                Console.WriteLine(i);
            }
            // FOREACH
            foreach(int i in arvud)
            {
                Console.WriteLine(i);
            }

            // KÕik tsüklid on tegelikultr FOR
            // WHILE ja FOR - samaväärsed tsüklid

            int i0 = -1;
            while ( arvud[++i0] != 5)
            {
                Console.WriteLine($"Variant 1 {i0} on {arvud[i0]}");
            }

            i0 = -1;
            for (; arvud[++i0] != 5;)
            {
                Console.WriteLine($"Variant 2 {i0} on {arvud[i0]}");
            }

            // DO ja FOR - samaväärsed tsüklid

            i0 = 0;
            do
            {
                Console.WriteLine($"Variant 3 {i0} on {arvud[i0]}");
            }
            while (arvud[++i0] != 5);

            i0 = 0;
            for (bool b = true; b; b = arvud[++i0] != 5)
            {
                Console.WriteLine($"Variant 4 {i0} on {arvud[i0]}");
            }

        }
    }
}
